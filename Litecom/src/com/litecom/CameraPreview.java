package com.litecom;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
/**
 * A simple wrapper around a Camera and a SurfaceView that renders a centered preview of the Camera
 * to the surface. We need to center the SurfaceView because not all devices have cameras that
 * support preview sizes at the same aspect ratio as the device's display.
 */
public class CameraPreview extends ViewGroup implements SurfaceHolder.Callback {
    private final String TAG = "Preview";

    SurfaceView _SurfaceView;
    SurfaceHolder _Holder;
    Size _PreviewSize;
    List<Size> _SupportedPreviewSizes;
    Camera _Camera;
    Context _Context;
    FrameProcessor _FrameProcessor;

    CameraPreview(Context iContext) {
        super(iContext);
        _Context = iContext;
    }

    public void Init(FrameProcessor iFrameProcessor)
    {
        _SurfaceView = new SurfaceView(_Context);
        addView(_SurfaceView);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        _Holder = _SurfaceView.getHolder();
        _Holder.addCallback(this);
        
        _FrameProcessor=iFrameProcessor;
    }
    public void setCamera(Camera camera) {
        _Camera = camera;
        if (_Camera != null) {
            _SupportedPreviewSizes = _Camera.getParameters().getSupportedPreviewSizes();
            requestLayout();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // We purposely disregard child measurements because act as a
        // wrapper to a SurfaceView that centers the camera preview instead
        // of stretching it.
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width , height);

        if (_SupportedPreviewSizes != null) {
            _PreviewSize = getOptimalPreviewSize(_SupportedPreviewSizes, width, height);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed && getChildCount() > 0) {
           final View child = getChildAt(0);

            final int width = r - l;
            final int height = b - t;

            int previewWidth = width;
            int previewHeight = height;
            if (_PreviewSize != null) {
                previewWidth = _PreviewSize.width;
                previewHeight = _PreviewSize.height;
            }

            // Center the child SurfaceView within the parent.
            if (width * previewHeight > height * previewWidth) {
                final int scaledChildWidth = previewWidth * height / previewHeight;
                child.layout(	(width - scaledChildWidth) / 2, 0,
                        		(width + scaledChildWidth) / 2, height);
            } else {
                final int scaledChildHeight = previewHeight * width / previewWidth;
                child.layout(	0, (height - scaledChildHeight) / 2,
                				width, (height + scaledChildHeight) / 2);
            }
        }
    }
      
    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        try 
        {
            if (_Camera != null) 
            {
                _Camera.setPreviewDisplay(holder);
                _Camera.setPreviewCallback(_FrameProcessor);
            }
        } 
        catch (IOException exception) 
        {
            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        if (_Camera != null) {
            _Camera.stopPreview();
        }
    }


    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        Camera.Parameters parameters = _Camera.getParameters();
        parameters.setPreviewSize(_PreviewSize.width, _PreviewSize.height);
        parameters.setPictureFormat( ImageFormat.JPEG );
        parameters.setPreviewFormat( ImageFormat.YV12 );
        requestLayout();
        _Camera.setPreviewCallback(_FrameProcessor);
        _Camera.setParameters(parameters);
        _Camera.startPreview();
        
        /*


        _Camera.setDisplayOrientation ( 90 );

        _Camera.startPreview();
        
        */
    }

}