package com.litecom;

public class BinaryNode<E> {
	private E _Item;
	private BinaryNode<E> _LeftNode;
	private BinaryNode<E> _RightNode;
	
	public BinaryNode(E iItem)
	{
		_Item = iItem;
	}
	
	public BinaryNode(E iItem, BinaryNode<E> iLeft, BinaryNode<E> iRight)
	{
		_Item = iItem;
		_LeftNode = iLeft;
		_RightNode = iRight;
	}
	
	public E GetItem()
	{
		return _Item;
	}
	
	public BinaryNode<E> GetLeft()
	{
		return _LeftNode;
	}
	
	public BinaryNode<E> GetRight()
	{
		return _RightNode;
	}
	
	public boolean IsLeaf()
	{
		return _LeftNode == null && null == _RightNode;
	}
	
	public void SetItem(E iItem)
	{
		_Item = iItem;
	}
	
	public void SetLeft(BinaryNode<E> iLeft)
	{
		_LeftNode = iLeft;
	}

	public void SetRight(BinaryNode<E> iRight)
	{
		_RightNode = iRight;
	}
}
