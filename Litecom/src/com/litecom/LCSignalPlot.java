package com.litecom;

import java.text.DecimalFormat;
import java.util.LinkedList;

import android.graphics.Color;
import android.widget.Button;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.LineAndPointRenderer;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYStepMode;


public class LCSignalPlot implements LCFrameObserverItf{

    private XYPlot _SignalPlot;    
    private SimpleXYSeries _SignalSeries;
    private LinkedList<Number> _Signals;
    private final int _MaxSignalSampleSize = 40;
    private boolean _PlotOn = true;
    private Button _Button;
    
    LCSignalPlot(XYPlot iPlotArea )
    {        

        // get handles to our View defined in layout.xml:
        _SignalPlot = iPlotArea;
 
        // only display whole numbers in domain labels
        _SignalPlot.getGraphWidget().setDomainValueFormat(new DecimalFormat("0"));
        //LitecomSignalSeries SignalSeries = new LitecomSignalSeries(_Decoder);
        _SignalSeries = new SimpleXYSeries("Signal Series");
        _SignalPlot.addSeries(_SignalSeries, LineAndPointRenderer.class, new LineAndPointFormatter(Color.rgb(100, 200, 100), Color.BLACK, Color.rgb(100, 200, 100)));

        //_SignalPlot.setGridPadding(5, 0, 5, 0);
        //_SignalPlot.setDomainStepMode(XYStepMode.SUBDIVIDE);
        //_SignalPlot.setDomainStepValue(1);
        _SignalPlot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1);
        _SignalPlot.setDomainValueFormat(new DecimalFormat("#"));
        // thin out domain/range tick labels so they dont overlap each other:
        _SignalPlot.setTicksPerDomainLabel(10);
        _SignalPlot.setTicksPerRangeLabel(50);
        _SignalPlot.disableAllMarkup();
        //_SignalPlot.setRangeBoundaries(0, 255, BoundaryMode.FIXED);
        //_SignalPlot.setDomainBoundaries(0, 50, BoundaryMode.FIXED);
    	_Signals = new LinkedList<Number>();
    	Reset();
    }
    
	public void PushData(int iSampleValue) 
	{
		if(_PlotOn) 
		{	
			if(_Signals.size() >=  _MaxSignalSampleSize)
				_Signals.removeFirst();
			
			_Signals.addLast(iSampleValue);
			this._SignalSeries.setModel(_Signals,  SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
		}
		
		this._SignalPlot.redraw();
	}	
	
	public void Reset()
	{
		_Signals.clear();
    	for(int ii = 0; ii <_MaxSignalSampleSize ; ii++)
    	{
    		_Signals.add(0);
    	}
	}
	
}
