package com.litecom;

import java.util.ArrayList;
import android.util.Log;


public class LCMorseSignalProcessor implements LCFrameObserverItf {

   	private ArrayList<Integer> _SignalSequence;
	private String _TAG = "LitecomMorseDecoder";
	private short _DotLength ; // unit is sample point
	private short _BackgroundLevel; 
	private MorseCodeTree _Decoder;
	private static final String _Dot = ".";
	private static final String _Dash= "-";
	private static final String _Space= "-";
	
	LCMorseSignalProcessor()
	{
		_SignalSequence = new ArrayList<Integer>(1200); // 1200 sample point is 50s' buffer for a 24 frames/s video
		_Decoder = MorseCodeTree.GetDecoderInstance();
	}	
	
	public void PushData(int iSampleValue)
	{
		if(_SignalSequence == null )
		{
			Log.e(_TAG, "PushData: null _SignalSequence");
			return ; 
		}
		_SignalSequence.add(iSampleValue);
		Log.i(_TAG, "Sample area pxiel average: " + iSampleValue);        
	}
	
	public ArrayList<Integer> GetBinarizedSignal()
	{
		Binarize();
		return _SignalSequence;
	}
	
	public String GetDecodedMessage()
	{
		return _Decoder.Decode(_ConvertSignalSequenceToMorseCode());
		
	}
	
	private String _ConvertSignalSequenceToMorseCode()
	{
		Binarize();
		String MorseCode =new String ();
		ArrayList<Integer> DerivativeSequence = _Derivation(_SignalSequence);
		
		int StepCount = 0;
		for(int ii = 0 ; ii < DerivativeSequence.size();ii++)
		{
			int Derivative = DerivativeSequence.get(ii);
			if (Derivative > 0 )
			{
				if(StepCount > 3 )
				{
					MorseCode += _Space;
				}
				StepCount = 0 ;			
			}
			else if(Derivative < 0)
			{				
				if(StepCount<4)
					MorseCode += _Dot;
				else
				{
					MorseCode += _Dash;
				}
				StepCount = 0;
			}
			
			StepCount ++;
		
		}
				
		return MorseCode + _Space;
		
	}
	
	
	/*
	 * return null if input signal sequence is empty
	 * */
	private ArrayList<Integer> _Derivation(ArrayList<Integer> iSignals )
	{
		int Size = iSignals.size();
		if(Size <= 1 )
		{
			return null;
		}
		ArrayList<Integer> Derivative = new ArrayList<Integer>(iSignals.size()-1);
				
		for(int ii = 1 ; ii < Size ; ii++)
		{
			Derivative.add( iSignals.get(ii)-iSignals.get(ii-1));
		}		
		
		return Derivative;		
	}
	
	
	private void Binarize()
	{
		if(_SignalSequence.size() == 0 )
			return;
		// 1. Find max and min
		int Max = _SignalSequence.get(1), Min = _SignalSequence.get(1);
		
		for(int ii =0 ; ii < _SignalSequence.size() ; ii ++)
		{
			int SamplePoint = _SignalSequence.get(ii);
			
			if(SamplePoint> Max)
			{
				Max = SamplePoint;
			}
			else if (SamplePoint < Min)
			{
				Min = SamplePoint;
			}
			
			Log.i(_TAG, "Signal max value: " + Max +", min value: "+Min);
		}
		
		//2. Binaries the signal
		int ThreadHold = (Max + Min)/2;
		
		for(int ii =0 ; ii < _SignalSequence.size() ; ii ++)
		{
			int SamplePoint = _SignalSequence.get(ii);
			int BinarizedSignal = 0;
			if(SamplePoint>= ThreadHold)
			{
				BinarizedSignal = 1;
			}
			else 
			{
				BinarizedSignal = 0;
			}
			_SignalSequence.set(ii, BinarizedSignal);
			
			Log.i(_TAG, "Signal : "+BinarizedSignal);
		}			
	}
	
	public void Reset()
	{
		_SignalSequence.clear();
	}
	
}


