/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.litecom;

import java.io.IOException;    
import java.util.List;

import com.androidplot.xy.XYPlot;

import android.annotation.TargetApi;
import android.app.Activity;    
import android.content.pm.ActivityInfo;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;    
import android.hardware.Camera;      
import android.hardware.Camera.Size;
import android.os.Bundle;    
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;    
import android.view.SurfaceView;    
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.view.Window;    

import android.util.DisplayMetrics;


public class LitecomMainActivity extends Activity implements SurfaceHolder.Callback, View.OnClickListener, Camera.AutoFocusCallback
{
        
    SurfaceHolder _SurfaceHolder;    
    Camera _Camera;    
    boolean isClicked = false;    
    
    FrameProcessor _FrameProcessor;
    LCMorseSignalProcessor _MorseSignalProcessor;
    
    SurfaceView _SurfaceView;//surfaceView   
    ImageView _SampleAreaIndicator;
    List<Camera.Size> _SupportedPreviewSizes;
    Size _OptimizedSize;
    LCSignalPlot _SignalPlot;
    LCUIController _UIController;

    /** Called when the activity is first created. */    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {    
        super.onCreate(savedInstanceState);
        
        // Set Window features
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    	super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_litecom_main);        
        
        _SampleAreaIndicator = (ImageView) findViewById(R.id.SampleAreaIndicator);
        _SurfaceView = (SurfaceView)findViewById(R.id.CameraPreview);
        
        _SurfaceHolder = _SurfaceView.getHolder();    

        _SurfaceHolder.addCallback(this);    
        _SurfaceView.setOnClickListener(this);
        
        LCSignalPlot _SignalPlot = new LCSignalPlot((XYPlot) findViewById(R.id.SignalPlot));
        
        _MorseSignalProcessor = new LCMorseSignalProcessor();
        _FrameProcessor = new FrameProcessor();
        _FrameProcessor.AddListenner(_MorseSignalProcessor);
        _FrameProcessor.AddListenner(_SignalPlot);
        _FrameProcessor.SetDebugViewer((TextView)findViewById(R.id.DebugInfo));
        _UIController = new  LCUIController(this,_SignalPlot,_FrameProcessor,_MorseSignalProcessor);
        
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu iMenu)    
    {
    	
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, iMenu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.Settings:
                //newGame();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
        
    // Surface holder call back 
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        Camera.Parameters params = _Camera.getParameters();
        params.setPictureFormat( ImageFormat.JPEG );
        params.setPreviewFormat( ImageFormat.YV12 );
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
        
        _SupportedPreviewSizes = _Camera.getParameters().getSupportedPreviewSizes();
        _OptimizedSize = getOptimalPreviewSize(_SupportedPreviewSizes);
        LinearLayout.LayoutParams SurfaceViewParams = new LinearLayout.LayoutParams(_OptimizedSize.height, _OptimizedSize.width);
        _SurfaceView.setLayoutParams(SurfaceViewParams);
        params.setPreviewSize(_OptimizedSize.width,_OptimizedSize.height); // The width here is the width in horizontal position
        _Camera.setParameters(params);
        _Camera.setDisplayOrientation ( 90 );
        _Camera.setPreviewCallback(_FrameProcessor);
        _Camera.startPreview();
        
        // Calculate the real position of SampleAreaIndicator according to the screen resolution
        FrameLayout.LayoutParams SampleAreaParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        
        SampleAreaParams.leftMargin = ( _OptimizedSize.height - _SampleAreaIndicator.getDrawable().getIntrinsicHeight())/2 ;
        SampleAreaParams.topMargin = (_OptimizedSize.width -  _SampleAreaIndicator.getDrawable().getIntrinsicHeight())/2;
        _SampleAreaIndicator.setLayoutParams(SampleAreaParams);
    }
    
    // Surface holder call back
    public void surfaceCreated(SurfaceHolder iHolder)
    {
        if(_Camera == null)    
        {
            _Camera = Camera.open();
            try 
            {
            	_Camera.setPreviewDisplay(iHolder);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    // Surface holder call back
    public void surfaceDestroyed(SurfaceHolder iHolder)
    {
    	_Camera.setPreviewCallback(null);
        _Camera.stopPreview();
        _Camera.release();
        _Camera = null;     
    }
    
    // On click call back    
    public void onClick(View v)
    {    
        if(!isClicked)
        {
            _Camera.autoFocus(this);  
            isClicked = true;    
        }
        else    
        {
            _Camera.startPreview();    
            isClicked = false;
        }            
    }
    
    // AutoFocus call back     
    public void onAutoFocus(boolean success, Camera camera)
    { 
        if(false && success)    
        {
            Camera.Parameters params = _Camera.getParameters();
            params.setPictureFormat(PixelFormat.JPEG);
            params.setPreviewSize(640,480);
            _Camera.setParameters(params);
            //_Camera.takePicture(null, null, jpeg);    
        }       
    }
        
    private Size getOptimalPreviewSize(List<Size> sizes)
    {
    	DisplayMetrics Metric = new DisplayMetrics();
    	getWindowManager().getDefaultDisplay().getMetrics(Metric);

        if (sizes == null) return null;
        Size optimalSize = null;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) 
        {
            if (size.height <= Metric.widthPixels && size.width <= Metric.heightPixels) 
            {
        		 optimalSize = size;
        		 break;
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) 
        {
        	int targetHeight = Metric.heightPixels;
            double minDiff = Double.MAX_VALUE;
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) 
            {
                if (Math.abs(size.height - targetHeight) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }
}
