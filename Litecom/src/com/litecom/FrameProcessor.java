package com.litecom;


import java.util.ArrayList;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.Log;
import android.widget.TextView;
import java.util.Date;


public class FrameProcessor implements PreviewCallback 
{
	private String _TAG = "FrameProcessor";
	private final static int _SampleAreaSize = 50;
	private ArrayList<LCFrameObserverItf> _Decoders;
	private int[] 		_Histogram;
	private TextView 	_DebugViewer;
	private boolean		_CaptureON;
	private Date		_LastFrameEndTime;
	
	FrameProcessor()
	{
		_Decoders = new ArrayList<LCFrameObserverItf>();
		_CaptureON = false;
	}
	
	public void SetDebugViewer(TextView iDebugInfoViewer)
	{
		_DebugViewer = iDebugInfoViewer;
	}
	
	public void AddListenner (LCFrameObserverItf iDecoder)
	{
		if(iDecoder != null)
			_Decoders.add(iDecoder);
		else
			Log.e(_TAG, "SetDecoder: null iDecoder");
	}

	public void onPreviewFrame(byte[] iFrame, Camera iCurrentCamera) 
	{
		
		Date  BeginTime  = new Date();
		int FramePeriod = 1;
		if (_LastFrameEndTime != null )
			FramePeriod =  BeginTime.compareTo(_LastFrameEndTime);
		if( 0 == FramePeriod )
			FramePeriod = 1;
		
		if(_Decoders == null)
		{
			Log.e(_TAG, "SetDecoder: null _Decoder");	
			return ;
		}

		Parameters Param = iCurrentCamera.getParameters();
		Size PreviewSize = Param.getPreviewSize();
		
		int WidthCenter = PreviewSize.width/2;
		int HeightCenter = PreviewSize.height/2;
		
		//
		try
		{
			//well, we pass the height instead of width. because the image buffer is organized by the height resolution.
			byte [] Lumi = ExtractLuminanceOfSampleArea(iFrame, WidthCenter - _SampleAreaSize/2, HeightCenter-_SampleAreaSize/2, _SampleAreaSize, PreviewSize.height); 
			
			
			//ComputeHistogram(Lumi);
			short[] ImageStat  = Stat(Lumi);
			short Threadhold = (short) (ImageStat[1]*0.9);
			short [] BinaryImage = Binarize(Lumi, Threadhold);
			int[] Center = CalculateCentroid(BinaryImage, _SampleAreaSize, _SampleAreaSize);
			int PixelSum =Sum(Lumi);
			// inform listener 
			for(int ii = 0 ; ii < _Decoders.size() && _CaptureON ; ii ++)
				//_Decoders.get(ii).PushData(ImageStat[0]);
				_Decoders.get(ii).PushData(PixelSum);
			
			
			// Stats
			_LastFrameEndTime= new Date();
			int Duration =  _LastFrameEndTime.compareTo(BeginTime);
			if(_DebugViewer != null)
			{
				_DebugViewer.setText("Frame sampling frequency: " + 1000/FramePeriod + 
						" Hz.\nFrame processing time: "+ Duration +" ms\n"+ 
						"Mean: " + ImageStat[0] + " Max: " + ImageStat[1] + " Min: " + ImageStat[2]);
			}
		}
		catch (Exception e)
		{
			Log.v("System.out", e.toString());
		}
		

		
		
		
	}
	
	private byte[] ExtractLuminanceOfSampleArea(byte[] iYU12, int iBeginX , int iBeginY, int iEdgeLength, int iXResolution)
	{
		byte[] SampleAreaLumia = new byte[_SampleAreaSize*_SampleAreaSize];
		for(int ii = 0 ; ii < iEdgeLength ; ii++ )
		{
			System.arraycopy(iYU12, iBeginX*(ii+iBeginY), SampleAreaLumia, ii*iEdgeLength, iEdgeLength);
		}
		return SampleAreaLumia;		
	}
	
	// return value is a 3 shorts array ,mean , max ,min of the image
	private short[] Stat(byte[] iArray)
	{
		
		short Max = 0 , Min = 255;
		int Sum = 0;
		for(int ii = 0 ; ii < iArray.length ; ii++)
		{
			short Pixel = (short) (iArray[ii] & 0xFF);

			Sum += Pixel;
			
			if(Pixel > Max )
				Max  = Pixel;
			if(Pixel < Min)
				Min = Pixel;
		}
				
		short [] ReturnValue = new short[3];
		ReturnValue[0] = (short)  (Sum / iArray.length);
		ReturnValue[1] = Max;
		ReturnValue[2] = Min;
		
		return ReturnValue;
	}
	
	// return value is a 3 shorts array ,mean , max ,min of the image
		private int Sum(byte[] iArray)
		{					
			int Sum = 0;
			for(int ii = 0 ; ii < iArray.length ; ii++)
			{
				short Pixel = (short) (iArray[ii] & 0xFF);
				Sum += Pixel;			
			}
					
			return Sum;
		}
	
	
	private short[] Binarize(byte[] iArray, short iThreadHold )
	{		
		short BinarizedImage[] = new short[iArray.length];
		for(int ii = 0 ; ii < iArray.length ; ii++)
		{
			short Pixel = (short) (iArray[ii] & 0xFF);
			if(Pixel>iThreadHold )				
				BinarizedImage[ii] =   255;
			else
				BinarizedImage[ii] = 0; //need to do this to convert byte to int
		}
		
		return BinarizedImage;
	}
	
	private void ComputeHistogram(byte[] iImage)
	{
		// reset histogram
		_Histogram = new  int[256];
		
		// build histogram
		for(int ii = 0 ; ii < iImage.length ; ii++)
		{
			_Histogram[iImage[ii] & 0xFF]++; //need to do this to convert byte to int					
		}
	}
	
	private byte[] MedianFilter(byte[] iArray)
	{
		
		return iArray;
		
	}
	
	private int [] CalculateCentroid(short[] iImage, int iWidth, int iHeight)
	{
		short PixelValue = 255;
		int[] Center =new int[2];
		Center[0] = Center[1] = -1;
		int PixelNumber = iImage.length;
		if(PixelNumber != iWidth*iHeight)
		{
			Log.e(_TAG, "Total pixel number: " + PixelNumber + " not equals iWidth "+ iWidth +" * iHeight " +iHeight);
			return Center;
		}
		
		int XSum = 0; // int can hold sum of 8 Million pixel of 255
		int YSum = 0;
		for(int XIndex = 1;  XIndex <= iWidth ; XIndex++)
		{
			for( int YIndex = 1 ; YIndex <= iHeight ; YIndex ++)
			{
				if( PixelValue == iImage[(XIndex-1)*(YIndex)] )
				{
					XSum += XIndex;
					YSum += YIndex; 
				}
 			}
		}
		
		Center[0] = XSum / iImage.length;
		Center[1] = YSum / iImage.length;
		Log.i(_TAG, "Image weight center X: " + Center[0] + " Y: "+ Center[1]);
		return Center;
	}
	
	public void SwitchCapture()
	{
		_CaptureON = !_CaptureON;
	}
}
