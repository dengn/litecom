package com.litecom.test;


import com.litecom.MorseCodeTree;

import android.test.AndroidTestCase;

public class MorseCodeTreeTest extends AndroidTestCase{
	private final String ExpectedResult = "EISH54V3UF?_2ARL\"+.WP@J1'TNDB6-=X/KC;!YMGZ7,QO8:CH90";
	private final String MorseCode = ". .. ... .... ..... ....- ...- ...-- ..- ..-. ..--.. ..--.- ..--- .- .-. .-.. .-..-. .-.-. .-.-.- .-- .--. .--.-. .--- .---- .----. - -. -.. -... -.... -....- -...- -..- -..-. -.- -.-. -.-.-. -.-.-- -.-- -- --. --.. --... --..-- --.- --- ---.. ---... ---- ----. ----- ";

	protected void setUp() throws Exception {
		super.setUp();
	}
	public void testDecode() {
		MorseCodeTree MorseDecoder = MorseCodeTree.GetDecoderInstance();
		String Result = MorseDecoder.Decode(MorseCode);
		assertTrue(Result.equals(ExpectedResult));
	}

}
